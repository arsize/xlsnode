export const config: any = {
  startRow: 59, //从第几行开始,必填
  startColmn: 2, // 从第几列开始,必填
  filterCharge: "", //前端负责人名字,必填
  xlsxName: "", //表格名字,必填
  sheetName: "", // 具体表名,必填
  iteration: "", //迭代,必填
  // 只针对以下列筛选,des字段是云效导入必传字段不能更改(除了模块)
  selectColmn: [
    {
      col: 2,
      des: "模块",
      key: "module",
    },
    {
      col: 3,
      des: "标题",
      key: "title",
    },
    {
      col: 5,
      des: "描述", // 备注,生成在云效描述区
      key: "describe",
    },
    {
      col: 7,
      des: "负责人",
      key: "charge",
    },
    {
      col: 8,
      des: "状态", //默认都是待处理
      key: "status",
    },
    {
      col: 9,
      des: "计划开始时间", //时间格式yyyy-mm-ss
      key: "startTime",
    },
    {
      col: 10,
      des: "计划完成时间",
      key: "endTime",
    },
    {
      col: 11,
      des: "预计工时",
      key: "useTime",
    },
  ],
};
